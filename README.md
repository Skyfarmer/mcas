# Building instructions

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```
This creates two executables (*mcas* and *scas_benchmark*) in the ```build``` directory.

# Usage mcas

mcas accepts two parameters: --test and --benchmark ARRAY_SIZE MAX_THREADS K

--test: Runs basic unit tests for the mcas operation. Note that the assertions only work in debug builds and will not trigger
in release mode. Undefine ``NDEBUG`` manually if you want assertions in release mode.

--benchmark ARRAY_SIZE MAX_THREADS K: Runs the one-second benchmark. ARRAY_SIZE specifies the size of the test array.
MAX_THREADS specifies how many threads should be used. K defines the amount of words used for the MCAS-operations. Note
that results will automatically be written in a csv file in the current directory with the name "resultsK_ARRAY_SIZE".
In case the file already exists, data will simply be appended.

# Usage scas_benchmark

scas_benchmark accepts the following parameters: ARRAY_SIZE MAX_THREADS

ARRAY_SIZE specifies the size of the test array. MAX_THREADS defines how many threads should be used for the one second test.
Note that results will automatically be written in a csv file in the current directory with the name "resultsSCAS_ARRAY_SIZE".
In case the file already exists, data will simply be appended.

# Reproduction of results
In order to reproduce all the results presented in the paper, the used commits for the evaluation are accordingly tagged.
```git tag``` will list all available tags in this repository. The benchmark.sh and benchmark_scas.sh execute the entire
benchmark for the MCAS and SCAS respectively. Note that the scripts expect ```nproc``` to be available on the system. This is used
to determine the maximum amount of processors available. All results are stored in the current working directory starting
with ``results*``.