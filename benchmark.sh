#!/bin/bash

# Runs benchmark 50 times with the specified number of threads
benchmark() {
  for _ in {1..50}; do
    if ! ./mcas --benchmark 100000 "$1" "$2"; then
      echo "ERROR: " $1 " " $2 >> ERROR
    fi
  done
}

for i in $(seq 1 "$(nproc)"); do
  benchmark "$i" 4
  benchmark "$i" 14
  benchmark "$i" 24
  benchmark "$i" 34
  benchmark "$i" 44
  benchmark "$i" 54
done