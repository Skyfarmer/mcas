//
// Created by shimmelbauer on 10/7/21.
//
#include "mcas.h"

int cmp_word_descriptors(const void *a, const void *b) {
  const WordDescriptor *first = a;
  const WordDescriptor *second = b;

  return first->address > second->address ? 1 : -1;
}

//Sort WordDescriptors by memory
void sort_memory(MCASDescriptor *desc) {
  qsort(desc->words, desc->size, sizeof desc->words[0], cmp_word_descriptors);
}

bool mcas_apply(MCASDescriptor *mcas) {
  sort_memory(mcas);
#ifndef MEMORY_RC
  // This fence is required because settings from the creating thread
  // to the word itself might not be visible on weakly ordered architectures.
  atomic_thread_fence(memory_order_release);
#endif
  bool result = multi_compare_exchange(mcas);
#ifdef MEMORY_RC
  downgrade_descriptor(mcas, 1);
#endif
  return result;
}