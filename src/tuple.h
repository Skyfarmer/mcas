//
// Created by shimmelbauer on 9/15/21.
//

#ifndef MCAS__TUPLE_H_
#define MCAS__TUPLE_H_

#include <stdint.h>

typedef struct WordDescriptor WordDescriptor;

typedef enum TupleType {
  //Only contains a value
  VALUE,
  //Contains both a WordDescriptor and a value
  DESC_VALUE
} TupleType;

typedef struct Tuple {
  //Represents the type of the tuple
  TupleType type;
  //Contains the value
  uintptr_t value;
  //Contains the pointer to the WordDescriptor as it was read from memory. (Only valid when type is DESC_VALUE)
  uintptr_t wordPtr;
  //Contains a valid WordDescriptor. Remember to downgrade pointer before discarding the value.
  //Only valid when type is DESC_VALUE
  WordDescriptor *word;
} Tuple;

Tuple new_value_tuple(uintptr_t value);
Tuple new_desc_value_tuple(uintptr_t word, uintptr_t value);
uintptr_t get_content(Tuple *tuple);

#endif //MCAS__TUPLE_H_
