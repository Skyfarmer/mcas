#pragma clang diagnostic push
#pragma ide diagnostic ignored "openmp-use-default-none"
#pragma ide diagnostic ignored "performance-no-int-to-ptr"
//
// Created by Simon Himmelbauer on 28.09.21.
//

#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include <omp.h>
#include <printf.h>
#include <stdio.h>
#include <unistd.h>
#include "benchmark.h"
#include "../mcas.h"
#include "../utils.h"
#include "../git/gitsha1.h"

const static int PRE_ALLOC_NODES = 600000000;

extern _Thread_local size_t MCAS_FREED_OBJECTS;
extern _Thread_local size_t MCAS_HELPED;
extern _Thread_local size_t MCAS_FAILED_CAS;
extern _Thread_local size_t MCAS_SUCCESSFUL_CAS;

#ifdef MEMORY_EPOCH
extern atomic_size_t **OTHER_EPOCHS;
extern size_t NUM_THREADS;
extern _Thread_local atomic_size_t EPOCH;
extern _Thread_local size_t THREAD_ID;
#endif

#ifdef MEMORY_RC
extern _Thread_local int16_t NUM_THREADS;
extern _Thread_local size_t CAS_TOTAL_RETRIES;
extern _Thread_local size_t CAS_FAILED_RETRIES;
#endif

typedef struct {
  long value;
  atomic_uintptr_t next;
  atomic_uintptr_t prev;
} Node;

typedef struct {
  Node *head;
  Node *tail;
} DoubleLinkedList;

//ONLY WORKS IN SINGLE_THREADED MODE!
bool list_validate(const DoubleLinkedList *list) {
  Node *tmp = list->head;
  if (tmp->prev != 0) {
    return false;
  }

  Node *prev = NULL;
  while (tmp != NULL) {
    Node *next = PTR_CAST_TYPE(read_mcas(&tmp->next), Node *);
    //Check whether the links between two Nodes are correct.
    if (next != NULL && tmp != PTR_CAST_TYPE(read_mcas(&next->prev), Node *)) {
      return false;
    }
    //Check for the set invariant
    if (prev != NULL && next != NULL && prev->value > next->value) {
      return false;
    }

    prev = tmp;
    tmp = next;
  }

  return prev == list->tail ? true : false;
}

size_t list_size(const DoubleLinkedList *list) {
  size_t counter = 0;
  Node *n = list->head;
  while (n != NULL) {
    counter += 1;
    n = PTR_CAST_TYPE(read_mcas(&n->next), Node *);
  }

  return counter;
}

Node *list_get(DoubleLinkedList *list, const long element) {
  Node *n = list->head;
  while (n != NULL && n->value <= element) {
    if (n->value == element) {
      return n;
    }
    n = PTR_CAST_TYPE(read_mcas(&n->next), Node *);
  }
  return NULL;
}

bool list_delete_intern(Node *node) {
  Node *prev = PTR_CAST_TYPE(read_mcas(&node->prev), Node *);
  Node *next = PTR_CAST_TYPE(read_mcas(&node->next), Node *);
  if (next == NULL) {
    //Someone has already removed this node
    return false;
  }

  MCASDescriptor *mcas = new_mcas(3, true);

  mcas->words[0].address = &prev->next;
  mcas->words[0].old = PTR_CAST_VOID(node);
  mcas->words[0].new = PTR_CAST_VOID(next);

  mcas->words[1].address = &next->prev;
  mcas->words[1].old = PTR_CAST_VOID(node);
  mcas->words[1].new = PTR_CAST_VOID(prev);

  mcas->words[2].address = &node->next;
  mcas->words[2].old = PTR_CAST_VOID(next);
  mcas->words[2].new = 0;

  return mcas_apply(mcas);
}

bool list_delete(DoubleLinkedList *list, const long element) {
  Node *n = list_get(list, element);
  if (n) {
    return list_delete_intern(n);
  }

  return false;
}

bool list_insert_st(DoubleLinkedList *list, long value) {
  Node *n = malloc(sizeof *n);
  n->value = value;

  Node *current = list->head;
  Node *prev = NULL;
  while (current != NULL) {
    if (value < current->value) {
      n->prev = PTR_CAST_VOID(prev);
      n->next = PTR_CAST_VOID(current);
      prev->next = PTR_CAST_VOID(n);
      current->prev = PTR_CAST_VOID(n);
      return true;
    }

    if (value == current->value) {
      //Value already exists
      free(n);
      return false;
    }
    prev = current;
    current = PTR_CAST_TYPE(current->next, Node *);
  }

  //Must never be reached
  abort();
}

bool list_insert_mt_internal(DoubleLinkedList *list, long value, Node *n) {
  Node *current = list->head;
  Node *prev = NULL;
  while (current != NULL) {
    if (current->value > value) {
      n->prev = PTR_CAST_VOID(prev);
      n->next = PTR_CAST_VOID(current);
      MCASDescriptor *mcas = new_mcas(2, true);

      mcas->words[0].address = &prev->next;
      mcas->words[0].old = PTR_CAST_VOID(current);
      mcas->words[0].new = PTR_CAST_VOID(n);

      mcas->words[1].address = &current->prev;
      mcas->words[1].old = PTR_CAST_VOID(prev);
      mcas->words[1].new = PTR_CAST_VOID(n);

      if (!mcas_apply(mcas)) {
        return list_insert_mt_internal(list, value, n);
      }
      return true;
    }

    if (value == current->value) {
      return false;
    }

    prev = current;
    current = PTR_CAST_TYPE(read_mcas(&current->next), Node *);
    if (current == NULL && prev != list->tail) {
      //This node might have been deleted, go back
      current = PTR_CAST_TYPE(read_mcas(&prev->prev), Node *);
      prev = PTR_CAST_TYPE(read_mcas(&current->prev), Node *);
    }
  }

  //Must never be reached
  abort();
}

bool list_insert_mt(DoubleLinkedList *list, Node *n) {
  if (!list_insert_mt_internal(list, n->value, n)) {
    return false;
  }

  return true;
}

void benchmark_list(const size_t initListSize,
                    const int numThreads,
                    const size_t readDistribution) {
  DoubleLinkedList list = {NULL, NULL};
  list.head = malloc(sizeof *list.head);
  list.tail = malloc(sizeof *list.tail);

  list.head->next = PTR_CAST_VOID(list.tail);
  list.head->prev = 0;
  list.head->value = LONG_MIN;

  list.tail->next = 0;
  list.tail->prev = PTR_CAST_VOID(list.head);
  list.tail->value = LONG_MAX;

  unsigned int initSeed = time(NULL);
  for (size_t i = 0; i < initListSize; i++) {
    while (!list_insert_st(&list, rand_r(&initSeed) % ((long) initListSize * 2))) {}
  }

  assert(list_validate(&list));

  size_t counterReadThreads[numThreads];
  size_t counterInsertionThreads[numThreads];
  size_t counterInsertionSuccessfulThreads[numThreads];
  size_t counterDeletionThreads[numThreads];
  size_t counterDeletionSuccessfulThreads[numThreads];
  size_t numFreedObjects[numThreads];
  size_t numHelpedMcas[numThreads];
  size_t numSuccessfulCas[numThreads];
  size_t numFailedCas[numThreads];
  double averageFail[numThreads];

#pragma omp parallel default(shared) proc_bind(close)
  {
    int me = omp_get_thread_num();

#ifdef MEMORY_EPOCH
    //Initialize epochs
#pragma omp single
    {
      NUM_THREADS = omp_get_num_threads();
      OTHER_EPOCHS = malloc(NUM_THREADS * sizeof *OTHER_EPOCHS);
    }
    OTHER_EPOCHS[me] = &EPOCH;
    THREAD_ID = me;
#endif
#ifdef MEMORY_RC
    NUM_THREADS = (int16_t) numThreads;
#endif

    unsigned int seed = time(NULL);
    size_t counterRead = 0;
    size_t counterInsertion = 0;
    size_t counterInsertionSuccessful = 0;
    size_t counterDeletion = 0;
    size_t counterDeletionSuccessful = 0;

    Node *nodes = malloc((PRE_ALLOC_NODES / numThreads) * sizeof *nodes);

    double accum = 0;
#pragma omp barrier
    while (accum < ONE_SECOND) {
      if (rand_r(&seed) % 100 < readDistribution) {
        //Read

        //Select element
        long element = rand_r(&seed) % ((long) initListSize * 2);

        struct timespec start;
        clock_gettime(CLOCK_MONOTONIC, &start);

        list_get(&list, element);

        struct timespec end;
        clock_gettime(CLOCK_MONOTONIC, &end);
        accum += TIME_DIFF_MS(start, end);
        counterRead += 1;
      } else {
        //Update
        switch (rand_r(&seed) % 2) {
          case 0: {
            //Insert
            struct timespec start;
            clock_gettime(CLOCK_MONOTONIC, &start);

            nodes->value = rand_r(&seed) % ((long) initListSize * 2);
            if (list_insert_mt(&list, nodes)) {
              nodes += 1;
              counterInsertionSuccessful += 1;
            }

            struct timespec end;
            clock_gettime(CLOCK_MONOTONIC, &end);
            accum += TIME_DIFF_MS(start, end);

            counterInsertion += 1;
            break;
          }
          case 1: {
            //Deletion
            long element = rand_r(&seed) % ((long) initListSize * 2);

            struct timespec start;
            clock_gettime(CLOCK_MONOTONIC, &start);

            if (list_delete(&list, element)) {
              counterDeletionSuccessful += 1;
            }

            struct timespec end;
            clock_gettime(CLOCK_MONOTONIC, &end);
            accum += TIME_DIFF_MS(start, end);

            counterDeletion += 1;
            break;
          }
          default:abort();
        }
      }
    }
#ifdef MEMORY_EPOCH
#pragma omp barrier
    cleanup();
#pragma omp single
    {
      free(OTHER_EPOCHS);
    }
#endif

#ifdef MEMORY_RC
    averageFail[me] = (double) CAS_TOTAL_RETRIES / (CAS_FAILED_RETRIES == 0 ? 1. : (double) CAS_FAILED_RETRIES);
#endif

    counterReadThreads[me] = counterRead;
    counterInsertionThreads[me] = counterInsertion;
    counterInsertionSuccessfulThreads[me] = counterInsertionSuccessful;
    counterDeletionThreads[me] = counterDeletion;
    counterDeletionSuccessfulThreads[me] = counterDeletionSuccessful;
    numFreedObjects[me] = MCAS_FREED_OBJECTS;
    numHelpedMcas[me] = MCAS_HELPED;
    numSuccessfulCas[me] = MCAS_SUCCESSFUL_CAS;
    numFailedCas[me] = MCAS_FAILED_CAS;
  }

  if (!list_validate(&list)) {
    abort();
  }

  size_t totalRead = 0;
  size_t totalInsertion = 0;
  size_t totalInsertionSuccessful = 0;
  size_t totalDeletion = 0;
  size_t totalDeletionSuccessful = 0;
  size_t totalFreedObjects = 0;
  size_t totalHelpedMcas = 0;
  size_t totalSuccessfulCas = 0;
  size_t totalFailedCas = 0;
  double averageRetryCount = 0.;
  for (size_t i = 0; i < numThreads; i++) {
    totalRead += counterReadThreads[i];
    totalInsertion += counterInsertionThreads[i];
    totalInsertionSuccessful += counterInsertionSuccessfulThreads[i];
    totalDeletion += counterDeletionThreads[i];
    totalDeletionSuccessful += counterDeletionSuccessfulThreads[i];

    totalFreedObjects += numFreedObjects[i];
    totalHelpedMcas += numHelpedMcas[i];
    totalSuccessfulCas += numSuccessfulCas[i];
    totalFailedCas += numFailedCas[i];
    averageRetryCount += averageFail[i];
  }
  averageRetryCount /= numThreads;

  printf("Read/Inserted/Deleted: %zu/%zu/%zu (%f %%/ %f %%/ %f %%)\n",
         totalRead,
         totalInsertion,
         totalDeletion,
         (double) totalRead * 100. / (double) (totalRead + totalInsertion + totalDeletion),
         (double) totalInsertion * 100. / (double) (totalRead + totalInsertion),
         (double) totalDeletion * 100. / (double) (totalRead + totalInsertion + totalDeletion));
  printf("Successful insertions/deletions: %zu/%zu\n", totalInsertionSuccessful, totalDeletionSuccessful);
  printf("Total amount of operations: %zu\n", totalRead + totalInsertion + totalDeletion);
  printf("Size of list: %zu\n", list_size(&list));
  assert(list_size(&list) - 2 == initListSize + totalInsertionSuccessful - totalDeletionSuccessful);

  //Write results to file
  const size_t fileNameSize = 4096;
  char buffer[fileNameSize];
  sprintf(buffer, "list_%zu", initListSize);
  FILE *f = NULL;
  if (access(buffer, F_OK) == 0) {
    //File already exists, append results
    f = fopen(buffer, "aw");
  } else {
    f = fopen(buffer, "w");
    //Add header
    fprintf(f,
            "commit,total_read,total_insertion,total_deletion,num_threads,helped_mcas,total_cas,total_successful,average_retry\n");
  }

  fprintf(f,
          "%s,%zu,%zu,%zu,%i,%zu,%zu,%zu,%f\n",
          g_GIT_SHA1,
          totalRead,
          totalInsertion,
          totalDeletion,
          numThreads,
          totalHelpedMcas,
          totalSuccessfulCas + totalFailedCas,
          totalSuccessfulCas,
          averageRetryCount);
  fclose(f);
}
#pragma clang diagnostic pop