//
// Created by Simon Himmelbauer on 28.09.21.
//

#ifndef MCAS_SRC_LIST_BENCHMARK_BENCHMARK_H_
#define MCAS_SRC_LIST_BENCHMARK_BENCHMARK_H_
#include <stddef.h>

void benchmark_list(size_t initListSize,
                    int numThreads,
                    size_t readDistribution);

#endif //MCAS_SRC_LIST_BENCHMARK_BENCHMARK_H_
