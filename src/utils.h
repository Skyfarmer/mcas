//
// Created by shimmelbauer on 9/15/21.
//

#ifndef MCAS_SRC_UTILS_H_
#define MCAS_SRC_UTILS_H_
#include <stdint.h>
//The highest order bit flags a WordDescriptor
const static uintptr_t DESC_BIT = 0x1ULL << 63;
//Represents the 16 uppermost bits
const static uintptr_t HIGH_BITS = 0xFFFFULL << 48;
//Represents the 48 lowermost bits
const static uintptr_t LOW_BITS = ~HIGH_BITS;

#define PTR_CAST_VOID(v) (uintptr_t)(void *)(v)
#define PTR_CAST_TYPE(v, type) (type)(void *)(v)

#define TIME_DIFF_MS(start, end) ((((end).tv_sec) * 1000.0 + (end).tv_nsec * 1e-6) - (((start).tv_sec) * 1000.0 + (start).tv_nsec * 1e-6))
#define ONE_SECOND 1000.0

#endif //MCAS_SRC_UTILS_H_
