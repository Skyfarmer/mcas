//
// Created by Simon Himmelbauer on 21.09.21.
//

#ifndef MCAS_SRC_ZERO_ALLOC_MCAS_H_
#define MCAS_SRC_ZERO_ALLOC_MCAS_H_

#ifdef MEMORY_RC
#include "rc/mcas.h"
#elif MEMORY_EPOCH
#include "epoch/mcas.h"
#else
#include "leaky/mcas.h"
#endif

bool mcas_apply(MCASDescriptor *);

#endif //MCAS_SRC_ZERO_ALLOC_MCAS_H_
