//
// Created by shimmelbauer on 06.05.21.
//
#include "mcas.h"
#include "../tuple.h"

#include <stdatomic.h>
#include <assert.h>

_Thread_local size_t MCAS_FREED_OBJECTS = 0;
_Thread_local size_t MCAS_HELPED = 0;
_Thread_local size_t MCAS_FAILED_CAS = 0;
_Thread_local size_t MCAS_SUCCESSFUL_CAS = 0;

//Represents the uppermost bit
#define HIGH_BIT (0x1llu << (sizeof(uintptr_t) * 8 - 1))
//Set the uppermost bit
#define MARK(addr) (addr | HIGH_BIT)
//Unset the uppermost bit
#define UNMARK(addr) (addr & ~HIGH_BIT)

//Allocate a new MCASDescriptor with the specified amount
//of WordDescriptors.
MCASDescriptor *new_mcas(const size_t words, const bool autoFree) {
  (void) autoFree;
  MCASDescriptor *mcas = malloc(sizeof(MCASDescriptor));

  mcas->status = ACTIVE;
  mcas->size = words;
  mcas->words = malloc(sizeof(WordDescriptor) * words);

  for (size_t i = 0; i < mcas->size; i++) {
    WordDescriptor *word = &mcas->words[i];
    word->parent = mcas;
    word->address = NULL;
  }

  return mcas;
}

//Checks whether the target address contains a WordDescriptor.
bool is_descriptor(uintptr_t addr) {
  //Cast to Descriptor
  return (addr & HIGH_BIT) != 0;
}

//Read from the target memory location and return the value stored.
Tuple read_internal(atomic_uintptr_t *addr, MCASDescriptor *self) {
  uintptr_t val = atomic_load_explicit(addr, memory_order_relaxed);
  if (!is_descriptor(val)) {
    //A plain old value, simply return it
    return new_value_tuple(val);
  }
  // This fence is required because settings from the creating thread
  // to the word itself might not be visible on weakly ordered architectures.
  atomic_thread_fence(memory_order_acquire);

  //Found a descriptor
  WordDescriptor *word = PTR_CAST_TYPE(UNMARK(val), WordDescriptor *);
  MCASDescriptor *parent = word->parent;

  if (parent != self && atomic_load_explicit(&parent->status, memory_order_acquire) == ACTIVE) {
    MCAS_HELPED += 1;
    //MCAS-operation is still in progress
    multi_compare_exchange(parent);
    //Retry operation
    return read_internal(addr, self);
  }


  return atomic_load_explicit(&parent->status, memory_order_relaxed) == SUCCESSFUL ?
         new_desc_value_tuple(val, word->new) :
         new_desc_value_tuple(val, word->old);
}

//Read from the memory location. Returns the actual pointer stored
//there.
uintptr_t read_mcas(atomic_uintptr_t *address) {
  Tuple tuple = read_internal(address, NULL);
  return tuple.value;
}

//Apply a Multi-Word Compare and Swap
bool multi_compare_exchange(MCASDescriptor *desc) {
  //Assertion is technically not important, but it
  //notifies the user that the implementation is not lock-free.
  assert(atomic_is_lock_free(&desc->status));

  bool success = true;
  for (size_t i = 0; i < desc->size; i++) {
    //Current WordDescriptor
    WordDescriptor *word = &desc->words[i];
    assert(atomic_is_lock_free(&word->address));

    //Read from location
    Tuple result = read_internal(word->address, desc);
    uintptr_t content = get_content(&result);
    //If this word already points to the right place, move on
    if (content == PTR_CAST_VOID(word)) {
      continue;
    }
    //If the expected value is different, the MCAS fails
    if (result.value != word->old) {
      success = false;
      break;
    }
    //If MCAS-operation has already been finished, stop
    if (atomic_load_explicit(&desc->status, memory_order_relaxed) != ACTIVE) {
      break;
    }
    if (result.type == DESC_VALUE) {
      //Add mark if type is descriptor
      content = MARK(content);
    }
    //Try to install the pointer to my descriptor, if failed, retry
    if (!atomic_compare_exchange_strong_explicit(word->address,
                                                 &content,
                                                 MARK(PTR_CAST_VOID(word)),
                                                 memory_order_relaxed,
                                                 memory_order_relaxed)) {
      i -= 1;
      MCAS_FAILED_CAS += 1;
      continue;
    }
    MCAS_SUCCESSFUL_CAS += 1;
  }
  StatusType expected = ACTIVE;
  if (atomic_compare_exchange_strong_explicit(&desc->status,
                                              &expected,
                                              success ? SUCCESSFUL : FAILED,
                                              memory_order_release,
                                              memory_order_relaxed)) {
    //If I finalized this descriptor, mark it for reclamation
    MCAS_SUCCESSFUL_CAS += 1;
  } else {
    MCAS_FAILED_CAS += 1;
  }
  return atomic_load_explicit(&desc->status, memory_order_relaxed) == SUCCESSFUL;
}