#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-magic-numbers"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "mcas.h"
#include "array_benchmark/benchmark.h"
#include "list_benchmark/benchmark.h"

const size_t size_test_data = 10000;

//Generate a test array with the specified size
long *generate_test_data(size_t size) {
  long *data = malloc(sizeof(long) * size);
  for (size_t i = 0; i < size; i++) {
    data[i] = (long) i;
  }

  return data;
}

#ifdef NDEBUG
#define ASSERT_EQUAL(actual, expected) ((void)0);
#else
#define ASSERT_EQUAL(actual, expected) \
{                                      \
if((actual) != (expected)) {               \
      fprintf(stderr, "Line %i: Actual/Expected: %li/%li\n", __LINE__, (long) (actual), (long) (expected)); \
      abort(); \
} \
}
#endif

//Tests an MCAS operation on a single thread
void test_single(const long *test_data) {
  MCASDescriptor *desc = new_mcas(4, false);
  atomic_uintptr_t result[4];
  //Initialize target with data
  for (size_t i = 0; i < desc->size; i++) {
    result[i] = test_data[i];
  }

  //Initialize MCAS
  for (size_t i = 0; i < desc->size; i++) {
    desc->words[i].address = &result[i];
    desc->words[i].old = test_data[i];
    desc->words[i].new = test_data[i + desc->size];
  }

  multi_compare_exchange(desc);

  //Verify
  ASSERT_EQUAL(desc->status, SUCCESSFUL);
  for (size_t i = 0; i < desc->size; i++) {
    long actual = (long) read_mcas(desc->words[i].address);
    long expected = test_data[i + desc->size];
    ASSERT_EQUAL(actual, expected);
  }

  free(desc);
}

//Tests two MCAS operation on a single thread
//Both MCAS point to the same memory locations in order the verify that one of the MCAS
//definitely fails.
void test_two(const long *test_data) {
  MCASDescriptor *successful = new_mcas(4, false);
  MCASDescriptor *failed = new_mcas(4, false);
  atomic_uintptr_t result[4];
  for (size_t i = 0; i < successful->size; i++) {
    result[i] = test_data[i];
  }

  for (size_t i = 0; i < successful->size; i++) {
    successful->words[i].address = &result[i];
    successful->words[i].old = test_data[i];
    successful->words[i].new = test_data[i + successful->size];

    failed->words[i].address = &result[i];
    failed->words[i].old = test_data[i];
    failed->words[i].new = test_data[i + successful->size + failed->size];
  }

  multi_compare_exchange(successful);
  multi_compare_exchange(failed);

  ASSERT_EQUAL(successful->status, SUCCESSFUL);
  ASSERT_EQUAL(failed->status, FAILED);
  for (size_t i = 0; i < successful->size; i++) {
    long actual = (long) read_mcas(&result[i]);
    long expected = test_data[i + successful->size];
    ASSERT_EQUAL(actual, expected);
  }
  for (size_t i = 0; i < failed->size; i++) {
    long actual = (long) read_mcas(&result[i]);
    long expected = test_data[i + failed->size];
    ASSERT_EQUAL(actual, expected);
  }

  free(successful);
  free(failed);
}

//Test using multiple threads but each thread has its own MCAS where each MCAS points to the same memory locations
//NOTE: This tests uses as many as threads as the machine provides.
//Thus, make sure that your system provides at least two hardware threads.
void test_multithreaded_different_mcas(const long *testData, const size_t words) {
  int numProcs = omp_get_num_procs();
  MCASDescriptor **descriptors = malloc(numProcs * sizeof(MCASDescriptor *));
  for (size_t i = 0; i < numProcs; i++) {
    descriptors[i] = new_mcas(words, false);
  }

  atomic_uintptr_t *result = malloc(words * sizeof(atomic_uintptr_t));
  for (size_t i = 0; i < numProcs; i++) {
    for (size_t j = 0; j < descriptors[i]->size; j++) {
      descriptors[i]->words[j].address = &result[j];
      descriptors[i]->words[j].old = read_mcas(&result[j]);
      descriptors[i]->words[j].new = testData[i * words + j];
    }
  }
  int successful = 0;
#pragma omp parallel default(none) shared(testData, descriptors, result, successful)
  {
    int me = omp_get_thread_num();
    MCASDescriptor *desc = descriptors[me];
    if (multi_compare_exchange(desc)) {
      //Only one thread may be successful
      successful = me;
    }
  }
  ASSERT_EQUAL(descriptors[successful]->status, SUCCESSFUL)
  for (size_t j = 0; j < words; j++) {
    uintptr_t val = read_mcas(&result[j]);
    printf("%lu\n", val);
    ASSERT_EQUAL(val, testData[successful * words + j])
  }

  for (size_t i = 0; i < numProcs; i++) {
    if (i != successful) {
      ASSERT_EQUAL(descriptors[i]->status, FAILED)
    }
  }
  for (size_t i = 0; i < numProcs; i++) {
    free(descriptors[i]);
  }
  free(descriptors);
}

//Test using multiple threads but all threads share the same MCAS.
void test_multithreaded_same_mcas(const long *testData, size_t words) {
  MCASDescriptor *desc = new_mcas(words, false);

  atomic_uintptr_t *result = malloc(words * sizeof(atomic_uintptr_t));
  for (size_t i = 0; i < desc->size; i++) {
    desc->words[i].address = &result[i];
    desc->words[i].old = read_mcas(&result[i]);
    desc->words[i].new = testData[i];
  }

#pragma omp parallel default(none) shared(testData, result, desc, words)
  {
    multi_compare_exchange(desc);
  }

  ASSERT_EQUAL(desc->status, SUCCESSFUL);
  for (size_t i = 0; i < words; i++) {
    uintptr_t val = read_mcas(&result[i]);
    printf("%lu\n", val);
    ASSERT_EQUAL(val, testData[i])
  }

  free(desc);
}

int main(int argc, char **argv) {
  if (argc == 1) {
    printf("Specify either --test or --benchmark\n");
    return -2;
  }

  long *testData = NULL;
  if (strcmp(argv[1], "--test") == 0) {
    testData = generate_test_data(size_test_data);

    test_single(testData);
    test_two(testData);
    test_multithreaded_different_mcas(testData, 1024);
    test_multithreaded_same_mcas(testData, 64);
  } else if (strcmp(argv[1], "--bench-array") == 0) {
    size_t size = 1000;
    if (argc >= 3) {
      size = strtoul(argv[2], NULL, 10);
    } else {
      printf("No array size provided, falling back to default %zu\n", size);
    }

    int amountThreads;
    if (argc >= 4) {
      amountThreads = (int) strtoll(argv[3], NULL, 10);
    } else {
      amountThreads = omp_get_num_procs();
    }

    testData = generate_test_data(size);
    int k = 4;
    if (argc >= 5) {
      k = (int) strtol(argv[4], NULL, 10);
    } else {
      printf("No number of words provided, falling back to default %d\n", k);
    }

    omp_set_num_threads(amountThreads);
    benchmark_array(testData, k, size, amountThreads);
  } else if (strcmp(argv[1], "--bench-list") == 0) {
    size_t initListSize = 500;
    if (argc >= 3) {
      initListSize = strtoul(argv[2], NULL, 10);
    } else {
      printf("No initial list size provided, falling back to default (%zu)\n", initListSize);
    }
    int amountThreads;
    if (argc >= 4) {
      amountThreads = (int) strtol(argv[3], NULL, 10);
    } else {
      amountThreads = omp_get_num_procs();
    }

    size_t distReads = 80;
    if (argc >= 5) {
      distReads = strtoul(argv[4], NULL, 10);
    } else {
      printf("No read distribution provided, falling back to default (%zu)\n", distReads);
    }

    omp_set_num_threads(amountThreads);
    benchmark_list(initListSize, amountThreads, distReads);
  } else {
    printf("Unknown option: %s\n", argv[1]);
  }

  free(testData);

  return 0;
}

#pragma clang diagnostic pop