#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-magic-numbers"
//
// Created by shimmelbauer on 08.05.21.
//
#include <omp.h>
#include <stdio.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>

#define TIME_DIFF_MS(start, end) (((end.tv_sec) * 1000.0 + end.tv_nsec * 1e-6) - ((start.tv_sec) * 1000.0 + start.tv_nsec * 1e-6))
#define ONE_SECOND 1000.0

void test_cas(atomic_llong *test_data, size_t size, int numThreads) {
  size_t cas_counters[numThreads];
  size_t success_counters[numThreads];

#pragma omp parallel default(none), shared(test_data, cas_counters, success_counters, size), proc_bind(close)
  {
    int thread_num = omp_get_thread_num();
    unsigned int seed = time(NULL);

    struct timespec start;
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &start);
    clock_gettime(CLOCK_MONOTONIC, &end);
    size_t counter = 0;
    size_t successful = 0;
#pragma omp barrier

    while (TIME_DIFF_MS(start, end) < ONE_SECOND) {
      size_t idx = rand_r(&seed) % size;

      long long value = atomic_load_explicit(&test_data[idx], memory_order_relaxed);
      if (atomic_compare_exchange_strong_explicit(&test_data[idx],
                                                  &value,
                                                  thread_num,
                                                  memory_order_relaxed,
                                                  memory_order_relaxed)) {
        successful += 1;
      }
      counter += 1;
      clock_gettime(CLOCK_MONOTONIC, &end);
    }
    cas_counters[thread_num] = counter;
    success_counters[thread_num] = successful;
  }

  size_t total = 0;
  size_t total_successful = 0;
  for (int i = 0; i < numThreads; i++) {
    double ratio = (double) success_counters[i] / (double) cas_counters[i];
    printf("Thread %d executed %zu CAS (%.4f %% successful)\n", i, cas_counters[i], ratio * 100);
    total += cas_counters[i];
    total_successful += success_counters[i];
  }
  printf("Executed %zu (%.4f %% successful) CAS-operations within 1 second\n",
         total,
         (double) total_successful * 100.0 / (double) total);

  //Write results to file
  char buffer[4096];
  sprintf(buffer, "resultsSCAS_%zu", size);
  FILE *f = NULL;
  if (access(buffer, F_OK) == 0) {
    //File already exists
    f = fopen(buffer, "aw");
  } else {
    f = fopen(buffer, "w");
    //Add header
    fprintf(f, "total_scas,total_successful,num_threads\n");
  }

  fprintf(f, "%li,%li,%i\n", total, total_successful, numThreads);
  fclose(f);
}

int main(int argc, char **argv) {
  static_assert(ATOMIC_LLONG_LOCK_FREE == 2, "Long long is not lock free");
  //Initialize randomizer
  if (argc == 1) {
    printf("%s: ARRAY_SIZE [ALL_THREADS]\n", argv[0]);
    return -3;
  }
  size_t size = strtoul(argv[1], NULL, 10);
  int threadCount = omp_get_num_procs();
  if (argc >= 3) {
    threadCount = (int) strtol(argv[2], NULL, 10);
  }

  atomic_llong *global_array = malloc(size * sizeof(atomic_llong));

  omp_set_num_threads(threadCount);
  test_cas(global_array, size, threadCount);

  free(global_array);

  return 0;
}
#pragma clang diagnostic pop