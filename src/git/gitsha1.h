//
// Created by Simon Himmelbauer on 04.10.21.
//

#ifndef MCAS_SRC_GIT_GITSHA1_H_
#define MCAS_SRC_GIT_GITSHA1_H_

// From https://stackoverflow.com/a/4318642/16084771
extern const char g_GIT_SHA1[];

#endif //MCAS_SRC_GIT_GITSHA1_H_
