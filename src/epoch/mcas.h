//
// Created by shimmelbauer on 06.05.21.
//

#ifndef MCAS_SRC_MCAS_H_
#define MCAS_SRC_MCAS_H_
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdatomic.h>

struct MCASDescriptor;

typedef struct WordDescriptor {
  //Target memory location
  atomic_uintptr_t *address;
  //Expected value
  uintptr_t old;
  //New value
  uintptr_t new;
  struct MCASDescriptor *parent;
} WordDescriptor;

typedef enum StatusType {
  ACTIVE,
  SUCCESSFUL,
  FAILED
} StatusType;

typedef struct MCASDescriptor {
  _Atomic StatusType status;
  //Amount of WordDescriptors stored in words.
  size_t size;
  WordDescriptor *words;
} MCASDescriptor;

MCASDescriptor *new_mcas(size_t words, bool auto_free);
bool multi_compare_exchange(MCASDescriptor *desc);
uintptr_t read_mcas(atomic_uintptr_t *address);
void cleanup();

#define PTR_CAST_VOID(v) (uintptr_t)(void *)(v)
#define PTR_CAST_TYPE(v, type) (type)(void *)(v)


#endif //MCAS_SRC_MCAS_H_
