//
// Created by shimmelbauer on 9/22/21.
//

#include <stdlib.h>
#include <assert.h>
#include "list.h"

void epoch_insert_list(EpochList *list, MCASDescriptor *element) {
  assert(list != NULL && element != NULL);

  EpochListElement *e = malloc(sizeof *e);
  e->next = list->head;
  e->element = element;
  list->head = e;
  list->count += 1;
}

MCASDescriptor *epoch_remove_first_list(EpochList *list) {
  assert(list != NULL);

  if (list->head == NULL) {
    return NULL;
  }

  EpochListElement *e = list->head;
  MCASDescriptor *ret = e->element;
  list->head = e->next;
  list->count -= 1;

  free(e);
  return ret;
}