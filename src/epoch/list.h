//
// Created by shimmelbauer on 9/22/21.
//

#ifndef MCAS_SRC_EPOCH_LIST_H_
#define MCAS_SRC_EPOCH_LIST_H_
#include <stddef.h>

typedef struct MCASDescriptor MCASDescriptor;

typedef struct EpochListElement {
  struct EpochListElement *next;
  MCASDescriptor *element;
} EpochListElement;

typedef struct {
  EpochListElement *head;
  size_t count;
} EpochList;


void epoch_insert_list(EpochList *, MCASDescriptor *);
MCASDescriptor *epoch_remove_first_list(EpochList*);

#endif //MCAS_SRC_EPOCH_LIST_H_
