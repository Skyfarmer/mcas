//
// Created by shimmelbauer on 06.05.21.
//
#include "mcas.h"
#include "../tuple.h"
#include "list.h"

#include <stdatomic.h>
#include <assert.h>
#include <sched.h>

_Thread_local size_t MCAS_FREED_OBJECTS = 0;
_Thread_local size_t MCAS_HELPED = 0;
_Thread_local size_t MCAS_FAILED_CAS = 0;
_Thread_local size_t MCAS_SUCCESSFUL_CAS = 0;

bool multi_compare_exchange_internal(MCASDescriptor *desc, bool);

//Represents the uppermost bit
#define HIGH_BIT (0x1LLU << 63)
//Set the uppermost bit
#define MARK(addr) ((addr)| HIGH_BIT)
//Unset the uppermost bit
#define UNMARK(addr) ((addr) & ~HIGH_BIT)

//Allocate a new MCASDescriptor with the specified amount
//of WordDescriptors.
MCASDescriptor *new_mcas(const size_t words, const bool autoFree) {
  (void) autoFree;
  MCASDescriptor *mcas = malloc(sizeof(MCASDescriptor));

  mcas->status = ACTIVE;
  mcas->size = words;
  mcas->words = malloc(sizeof(WordDescriptor) * words);

  for (size_t i = 0; i < mcas->size; i++) {
    WordDescriptor *word = &mcas->words[i];
    word->parent = mcas;
    word->address = NULL;
  }

  return mcas;
}

//Checks whether the target address contains a WordDescriptor.
bool is_descriptor(uintptr_t addr) {
  //Cast to Descriptor
  return (addr & HIGH_BIT) != 0;
}

//Read from the target memory location and return the value stored.
Tuple read_internal(atomic_uintptr_t *addr, MCASDescriptor *self) {
  uintptr_t val = atomic_load_explicit(addr, memory_order_relaxed);
  if (!is_descriptor(val)) {
    //A plain old value, simply return it
    return new_value_tuple(val);
  }
  // This fence is required because settings from the creating thread
  // to the word itself might not be visible on weakly ordered architectures.
  atomic_thread_fence(memory_order_acquire);

  //Found a descriptor
  WordDescriptor *word = PTR_CAST_TYPE(UNMARK(val), WordDescriptor *);
  MCASDescriptor *parent = word->parent;
  if (parent != self && atomic_load_explicit(&parent->status, memory_order_acquire) == ACTIVE) {
    MCAS_HELPED += 1;
    //MCAS-operation is still in progress
    multi_compare_exchange_internal(parent, false);
    //Retry operation
    return read_internal(addr, self);
  }

  return atomic_load_explicit(&parent->status, memory_order_relaxed) == SUCCESSFUL ?
         new_desc_value_tuple(val, word->new) :
         new_desc_value_tuple(val, word->old);
}

atomic_size_t **OTHER_EPOCHS = NULL;
size_t NUM_THREADS = 0;
_Thread_local atomic_size_t EPOCH = 0;
_Thread_local size_t THREAD_ID = 0;
_Thread_local EpochList finalized_desc_list;
_Thread_local EpochList detached_desc_list;
_Thread_local size_t *LAST_EPOCHS = NULL;

void epoch_start() {
  atomic_fetch_add_explicit(&EPOCH, 1, memory_order_acquire);
}
void epoch_end() {
  atomic_fetch_add_explicit(&EPOCH, 1, memory_order_release);
}

void replace_words(MCASDescriptor *desc) {
  for (size_t i = 0; i < desc->size; i++) {
    WordDescriptor *word = &desc->words[i];
    size_t addr = atomic_load_explicit(word->address, memory_order_relaxed);
    if (UNMARK(addr) == PTR_CAST_VOID(word)) {
      //WordDescriptor has been inserted, swap with previous value
      size_t value = atomic_load_explicit(&desc->status, memory_order_relaxed) == SUCCESSFUL ? word->new : word->old;
      //We don't need to check whether this CAS failed or not because the only reason it might fail
      //is because it has been replaced by something else inbetween
      if (atomic_compare_exchange_strong_explicit(word->address,
                                                  &addr,
                                                  value,
                                                  memory_order_relaxed,
                                                  memory_order_relaxed)) {
        MCAS_SUCCESSFUL_CAS += 1;
      } else {
        MCAS_FAILED_CAS += 1;
      }
    }
  }
}

void retire_for_cleanup(MCASDescriptor *desc) {
  epoch_insert_list(&finalized_desc_list, desc);
  //Check for threshold
  const size_t epochThreshold = 1000;
  if (finalized_desc_list.count >= epochThreshold) {
    if (LAST_EPOCHS == NULL) {
      LAST_EPOCHS = calloc(NUM_THREADS, sizeof *LAST_EPOCHS);
    }
    //Run through all thread epochs
    for (size_t i = 0; i < NUM_THREADS; i++) {
      if (i == THREAD_ID) {
        continue;
      }
      size_t current = atomic_load_explicit(OTHER_EPOCHS[i], memory_order_relaxed);
      while (current % 2 != 0 && current == LAST_EPOCHS[i]) {
        current = atomic_load_explicit(OTHER_EPOCHS[i], memory_order_relaxed);
        sched_yield();
      }
      LAST_EPOCHS[i] = current;
    }
    //All primary_detached_desc_list can be reclaimed
    for (MCASDescriptor *tmp = epoch_remove_first_list(&detached_desc_list); tmp;
         tmp = epoch_remove_first_list(&detached_desc_list)) {
      MCAS_FREED_OBJECTS += 1;
      free(tmp->words);
      free(tmp);
    }
    //All finalized_desc_List can be moved to primary_detached_desc_list by replacing values.
    for (MCASDescriptor *tmp = epoch_remove_first_list(&finalized_desc_list); tmp;
         tmp = epoch_remove_first_list(&finalized_desc_list)) {
      replace_words(tmp);
      //All WordDescriptors have been removed, add to secondary_detached_desc_list
      epoch_insert_list(&detached_desc_list, tmp);
    }
  }
}

//Read from the memory location. Returns the actual pointer stored
//there.
uintptr_t read_mcas(atomic_uintptr_t *address) {
  epoch_start();
  Tuple tuple = read_internal(address, NULL);
  epoch_end();
  return tuple.value;
}

//Apply a Multi-Word Compare and Swap
bool multi_compare_exchange_internal(MCASDescriptor *desc, bool isFirst) {
  if (isFirst) {
    epoch_start();
  }
  //Assertion is technically not important, but it
  //notifies the user that the implementation is not lock-free.
  assert(atomic_is_lock_free(&desc->status));

  bool success = true;
  for (size_t i = 0; i < desc->size; i++) {
    //Current WordDescriptor
    WordDescriptor *word = &desc->words[i];
    assert(atomic_is_lock_free(&word->address));

    //Read from location
    Tuple result = read_internal(word->address, desc);
    uintptr_t content = get_content(&result);
    //If this word already points to the right place, move on
    if (content == PTR_CAST_VOID(word)) {
      continue;
    }
    //If the expected value is different, the MCAS fails
    if (result.value != word->old) {
      success = false;
      break;
    }
    //If MCAS-operation has already been finished, stop
    if (atomic_load_explicit(&desc->status, memory_order_relaxed) != ACTIVE) {
      break;
    }
    if (result.type == DESC_VALUE) {
      //Add mark if type is descriptor
      content = MARK(content);
    }
    //Try to install the pointer to my descriptor, if failed, retry
    if (!atomic_compare_exchange_strong_explicit(word->address,
                                                 &content,
                                                 MARK(PTR_CAST_VOID(word)),
                                                 memory_order_relaxed,
                                                 memory_order_relaxed)) {
      MCAS_FAILED_CAS += 1;
      i -= 1;
      continue;
    }
    MCAS_SUCCESSFUL_CAS += 1;
  }
  StatusType expected = ACTIVE;
  if (atomic_compare_exchange_strong_explicit(&desc->status,
                                              &expected,
                                              success ? SUCCESSFUL : FAILED,
                                              memory_order_release,
                                              memory_order_relaxed)) {
    MCAS_SUCCESSFUL_CAS += 1;
    //If I finalized this descriptor, mark it for reclamation
    retire_for_cleanup(desc);
  } else {
    MCAS_FAILED_CAS += 1;
    //Status has already been set, retrieve result
    success = expected == SUCCESSFUL;
  }
  if (isFirst) {
    epoch_end();
  }

  return success;
}

bool multi_compare_exchange(MCASDescriptor *desc) {
  return multi_compare_exchange_internal(desc, true);
}

void cleanup() {
  //Remove all descriptors in lists
  for (MCASDescriptor *tmp = epoch_remove_first_list(&detached_desc_list); tmp;
       tmp = epoch_remove_first_list(&detached_desc_list)) {
    MCAS_FREED_OBJECTS += 1;

    free(tmp->words);
    free(tmp);
  }
  for (MCASDescriptor *tmp = epoch_remove_first_list(&finalized_desc_list); tmp;
       tmp = epoch_remove_first_list(&finalized_desc_list)) {
    MCAS_FREED_OBJECTS += 1;

    replace_words(tmp);

    free(tmp->words);
    free(tmp);
  }
  free(LAST_EPOCHS);
}