//
// Created by shimmelbauer on 9/15/21.
//
#include <stddef.h>
#include "tuple.h"
#include "utils.h"

//Create a new tuple containing only a value
Tuple new_value_tuple(uintptr_t value) {
  Tuple tuple;
  tuple.type = VALUE;
  tuple.value = value;
  tuple.wordPtr = 0;
  tuple.word = NULL;

  return tuple;
}

//Create a new tuple containing a WordDescriptor and a value
Tuple new_desc_value_tuple(uintptr_t word, uintptr_t value) {
  Tuple tuple;
  tuple.type = DESC_VALUE;
  tuple.value = value;
  tuple.wordPtr = word;
  tuple.word = PTR_CAST_TYPE(word & LOW_BITS, WordDescriptor *);

  return tuple;
}

//Returns the actual content at the memory location, which
//is either a value or a WordDescriptor.
uintptr_t get_content(Tuple *tuple) {
  switch (tuple->type) {
    case VALUE:return tuple->value;
    case DESC_VALUE:return tuple->wordPtr & LOW_BITS;
  }
}