//
// Created by shimmelbauer on 06.05.21.
//
#include "mcas.h"
#include "../utils.h"
#include "../tuple.h"
#include <stdatomic.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

_Thread_local size_t MCAS_FREED_OBJECTS = 0;
_Thread_local size_t MCAS_HELPED = 0;
_Thread_local size_t MCAS_FAILED_CAS = 0;
_Thread_local size_t MCAS_SUCCESSFUL_CAS = 0;
_Thread_local size_t CAS_TOTAL_RETRIES = 0;
_Thread_local size_t CAS_FAILED_RETRIES = 0;

_Thread_local int16_t NUM_THREADS = -1;

#define UNUSED_SHIFT 48
//Unset the uppermost bits
#define UNMARK(addr) ((addr) & ~HIGH_BITS)
//Increase pointer counter by one
#define UPGRADE_CTR(v) ((v) + (0x1ull << UNUSED_SHIFT))
//Decrease pointer counter by one
#define DOWNGRADE_CTR(v) ((v) - (0x1ull << UNUSED_SHIFT))
//Extract the counter stored within the uppermost bits (except for the most significant one)
#define GET_COUNTER(v) (((v) & (DESC_BIT ^ HIGH_BITS)) >> UNUSED_SHIFT)

//Allocate a new MCASDescriptor with the specified amount
//of WordDescriptors.
MCASDescriptor *new_mcas(const size_t words, const bool autoFree) {
  MCASDescriptor *mcas = calloc(1, sizeof *mcas);
  mcas->status = ACTIVE;
  mcas->size = words;
  mcas->words = malloc(sizeof(WordDescriptor) * words);
  mcas->autoFree = autoFree;

  for (size_t i = 0; i < mcas->size; i++) {
    WordDescriptor *word = &mcas->words[i];
    word->parent = mcas;
    word->address = NULL;
  }
  atomic_store_explicit(&mcas->rc, 1, memory_order_release);

  return mcas;
}

void free_mcas(MCASDescriptor *desc) {
  MCAS_FREED_OBJECTS += 1;

  free(desc->words);
  if (desc->autoFree) {
    free(desc);
  }
}

//Checks whether two pointers are equal by only considering the relevant 48 least significant bits
bool is_ptr_equal(const uintptr_t f, const uintptr_t s) {
  return (f & LOW_BITS) == (s & LOW_BITS);
}

//Upgrades the live reference counter by the amount of references specified
//If the rc was previously 0, it additionally notifies its parent that this
//WordDescriptor is in use again.
//We do not need to consider the stored RC because this can only be upgraded
//by the thread swapping out the WordDescriptor.
void upgrade_descriptor(MCASDescriptor *desc, const size_t amountReferences) {
  if (amountReferences == 0) {
    return;
  }
  atomic_fetch_add_explicit(&desc->rc, amountReferences, memory_order_acquire);
}

//Decrement the live reference counter by the amount specified.
void downgrade_descriptor(MCASDescriptor *desc, const size_t amountReferences) {
  if (amountReferences == 0) {
    return;
  }
  size_t counter = atomic_fetch_sub_explicit(&desc->rc, amountReferences, memory_order_release);

  //Cannot reduce the counter by more references than currently possible.
  //assert(counter >= amountReferences);
  if (counter == amountReferences) {
    //This was the last valid word descriptor within the MCASDescriptor, remove all WordDescriptors and
    //the MCASDescriptor
    free_mcas(desc);
  }
}

//Decrement the reference counter contained within the uppermost bits of the WordDescriptor pointer.
void downgrade_descriptor_persistent(uintptr_t descPtr, const size_t amountReferences) {
  WordDescriptor *desc = PTR_CAST_TYPE(descPtr & LOW_BITS, WordDescriptor *);
  atomic_uintptr_t *addr = desc->address;

  uintptr_t orig = descPtr;
  uintptr_t target = DOWNGRADE_CTR(descPtr);
  size_t retryCounter = 0;
  bool mustRetry = false;
  while (!atomic_compare_exchange_strong_explicit(addr, &orig, target, memory_order_acquire, memory_order_relaxed)) {
    MCAS_FAILED_CAS += 1;
    retryCounter += 1;
    mustRetry = true;
    //Downgrade failed
    //Check whether the installed value/descriptor is still the same
    if (!is_ptr_equal(target, orig)) {
      CAS_TOTAL_RETRIES += retryCounter;
      CAS_FAILED_RETRIES += 1;


      //Descriptor has already been swapped out
      //Remove the reference to the stored rc
      downgrade_descriptor(desc->parent, amountReferences);
      return;
    }
    //Pointer is still the same, reference counter must have changed then
    target = DOWNGRADE_CTR(orig);
  }

  if (mustRetry) {
    CAS_TOTAL_RETRIES += retryCounter;
    CAS_FAILED_RETRIES += 1;
  }

  MCAS_SUCCESSFUL_CAS += 1;
}

//Checks whether the target address contains a WordDescriptor.
bool is_descriptor(const uintptr_t addr) {
  //Cast to Descriptor
  return (addr & DESC_BIT) != 0;
}

//Read from the target memory location and return the value stored.
Tuple read_internal(atomic_uintptr_t *addr, MCASDescriptor *self) {
  uintptr_t val = atomic_load_explicit(addr, memory_order_relaxed);
  if (!is_descriptor(val)) {
    //A plain old value, simply return it
    return new_value_tuple(val);
  }
  //We have a descriptor, upgrade reference counter
  uintptr_t target = UPGRADE_CTR(val);
  //Insert upgraded counter
  size_t retryCounter = 0;
  bool mustRetry = false;
  while (!atomic_compare_exchange_strong_explicit(addr, &val, target, memory_order_relaxed, memory_order_relaxed)) {
    MCAS_FAILED_CAS += 1;
    retryCounter += 1;
    mustRetry = true;
    //Insertion failed, either some thread was faster when acquiring this WordDescriptor
    //or another WordDescriptor was inserted.
    //Anyway, val now contains the actual value, attempt to upgrade this one.
    //Question: What if val contains a plain value instead of a WordDescriptor?
    //Answer: This algorithm assumes that WordDescriptors are never removed, thus, once
    //a WordDescriptor has been injected, all future injections must also be WordDescriptors.
    target = UPGRADE_CTR(val);
  }

  if (mustRetry) {
    CAS_TOTAL_RETRIES += retryCounter;
    CAS_FAILED_RETRIES += 1;
  }

  MCAS_SUCCESSFUL_CAS += 1;
  //Descriptor was loaded and upgraded successfully
  WordDescriptor *word = PTR_CAST_TYPE(UNMARK(target), WordDescriptor *);
  //IDEA: If the previous live reference counter was 0, we can already repeat reading.
  //Why? Well because if the live reference counter was 0, then the WordDescriptor must have already been swapped out
  //in between the CAS above (because a stored WordDescriptor counts as a reference). --> TODO
  MCASDescriptor *parent = word->parent;

  if (parent != self && atomic_load_explicit(&parent->status, memory_order_acquire) == ACTIVE) {
    MCAS_HELPED += 1;
    //MCAS-operation is still in progress
    multi_compare_exchange(parent);
    //Free descriptor reference
    downgrade_descriptor_persistent(target, 1);
    //Retry operation
    return read_internal(addr, self);
  }

  return atomic_load_explicit(&parent->status, memory_order_relaxed) == SUCCESSFUL ?
         new_desc_value_tuple(target, word
             ->new) :
         new_desc_value_tuple(target, word
             ->old);
}

//Read from the memory location. Returns the actual pointer stored
//there.
uintptr_t read_mcas(atomic_uintptr_t *address) {
  Tuple tuple = read_internal(address, NULL);
  if (tuple.type == DESC_VALUE) {
    //We don't need the WordDescriptor, decrement rc...
    downgrade_descriptor_persistent(tuple.wordPtr, 1);
  }
  return tuple.value;
}

//Apply a Multi-Word Compare and Swap
bool multi_compare_exchange(MCASDescriptor *desc) {
  //Assertion is technically not important, but it
  //notifies the user that the implementation is not lock-free.
  assert(atomic_is_lock_free(&desc->status));
  assert(NUM_THREADS != -1);

  upgrade_descriptor(desc, desc->size);

  bool success = true;
  size_t inserted = 0;
  for (size_t i = 0; i < desc->size; i++) {
    //Current WordDescriptor
    WordDescriptor *word = &desc->words[i];
    assert(atomic_is_lock_free(&word->address));

    //Read from location
    Tuple result = read_internal(word->address, desc);
    uintptr_t content = get_content(&result);
    //If this word already points to the right place, move on
    if (content == PTR_CAST_VOID(word)) {
      assert(result.type == DESC_VALUE);
      //Remove reference to loaded WordDescriptor
      downgrade_descriptor_persistent(result.wordPtr, 1);

      continue;
    }
    //If the expected value is different, the MCAS fails
    if (result.value != word->old) {
      success = false;
      if (result.type == DESC_VALUE) {
        //Remove reference to loaded WordDescriptor
        downgrade_descriptor_persistent(result.wordPtr, 1);
      }
      break;
    }
    //If MCAS-operation has already finished, stop
    if (atomic_load_explicit(&desc->status, memory_order_relaxed) != ACTIVE) {
      if (result.type == DESC_VALUE) {
        //Remove reference to loaded WordDescriptor
        downgrade_descriptor_persistent(result.wordPtr, 1);
      }
      break;
    }
    if (result.type == DESC_VALUE) {
      //Use the pointer including the DESC_BIT as well as the integrated reference counter
      content = result.wordPtr;
      //Add a safety reference to prevent other threads to free this MCASDescriptor
      //in case when the CAS was successful.
      upgrade_descriptor(result.word->parent, 2 * NUM_THREADS);
    }
    //Mark our new WordDescriptor
    //The uppermost bits are zero anyways, so  we simply start the reference counter with 0.
    uintptr_t target = PTR_CAST_VOID(word) | DESC_BIT;

    retry:
    //Try to install the pointer to point to our WordDescriptor
    if (atomic_compare_exchange_strong_explicit(word->address,
                                                &content,
                                                target,
                                                memory_order_release,
                                                memory_order_relaxed)) {
      //We could successfully insert the WordDescriptor
      MCAS_SUCCESSFUL_CAS += 1;
      inserted += 1;
      if (result.type == DESC_VALUE) {
        //The counter inside the WordDescriptor pointer is the amount of threads which
        //have successfully loaded this WordDescriptor. (The live rc is not a precise indicator.)
        //Therefore, we increase the stored rc by exactly this amount as all other threads
        //will eventually decrease it back to 0 at some point in the execution.
        //Remaining references == PTR_RC - safety net - this thread's reference (1) - installed reference (1)
        int64_t amount = (int64_t) GET_COUNTER(content) - 2 * NUM_THREADS - 2;
        assert(amount < 0);
        downgrade_descriptor(result.word->parent, -amount);
      }
    } else {
      MCAS_FAILED_CAS += 1;
      if (result.type == DESC_VALUE) {
        //The CAS failed because some thread was either faster to insert the same or a
        //different WordDescriptor. However, it might also be that some thread read this location
        //and thus, increased the reference counter. In this, case, we can simply retry this CAS
        //with the new reference counter.
        if (is_ptr_equal(content, result.wordPtr)) {
          //Only the reference counter has changed, retry...
          goto retry;
        }
        //The previous descriptor got swapped out, simply drop the reference and retry the current iteration.
        //Remove own reference and safety net.
        downgrade_descriptor(result.word->parent, 1 + 2 * NUM_THREADS);
      }
      //Retry current iteration
      i -= 1;
    }
  }

  StatusType expected = ACTIVE;
  if (atomic_compare_exchange_strong_explicit(&desc->status,
                                              &expected,
                                              success ? SUCCESSFUL : FAILED,
                                              memory_order_release,
                                              memory_order_relaxed)) {
    MCAS_SUCCESSFUL_CAS += 1;
  } else {
    MCAS_FAILED_CAS += 1;
    //CAS failed, status must already have been changed
    success = expected == SUCCESSFUL;
  }

  downgrade_descriptor(desc, desc->size - inserted);

  return success;
}

//Cleans up any WordDescriptors from the test array.
//This is useful to check for any memory leaks but ensuring that the installed WordDescriptors are not counted.
void cleanup(atomic_uintptr_t *data, size_t size) {
  for (size_t i = 0; i < size; i++) {
    if (is_descriptor(data[i])) {
      WordDescriptor *desc = PTR_CAST_TYPE(data[i] & LOW_BITS, WordDescriptor *);
      assert(desc->parent->rc != 0);
      downgrade_descriptor(desc->parent, 1);
    }
  }
}
