#pragma clang diagnostic push
#pragma ide diagnostic ignored "openmp-use-default-none"
//
// Created by Simon Himmelbauer on 18.09.21.
//
#include "benchmark.h"
#include "../mcas.h"
#include "../utils.h"
#include "../git/gitsha1.h"
#include <stddef.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <unistd.h>
#include <memory.h>
#include <assert.h>

extern _Thread_local size_t MCAS_FREED_OBJECTS;
extern _Thread_local size_t MCAS_HELPED;
extern _Thread_local size_t MCAS_FAILED_CAS;
extern _Thread_local size_t MCAS_SUCCESSFUL_CAS;

#ifdef MEMORY_EPOCH
extern atomic_size_t **OTHER_EPOCHS;
extern size_t NUM_THREADS;
extern _Thread_local atomic_size_t EPOCH;
extern _Thread_local size_t THREAD_ID;
#endif

#ifdef MEMORY_RC
extern _Thread_local int16_t NUM_THREADS;
extern _Thread_local size_t CAS_TOTAL_RETRIES;
extern _Thread_local size_t CAS_FAILED_RETRIES;
#endif
//Floyd algorithm
//FROM https://stackoverflow.com/a/1608585/16084771
size_t *array_rand_unique_floyd(const size_t size, const size_t upper, unsigned int *seed) {
  size_t *indices = malloc(size * sizeof *indices);

  bool *isUsed = malloc(upper * sizeof(bool));
  memset(isUsed, 0, upper * sizeof(bool));
  size_t im = 0;

  for (size_t in = upper - size; in < upper && im < size; in++) {
    size_t r = rand_r(seed) % (in + 1);
    if (isUsed[r]) {
      r = in;
    }

    assert(!isUsed[r]);
    indices[im++] = r;
    isUsed[r] = true;
  }
  assert(im == size);

  free(isUsed);
  return indices;
}

size_t *array_rand_unique_naive(const size_t size, const size_t upper, unsigned int *seed) {
  size_t *indices = malloc(size * sizeof(size_t));

  for (size_t i = 0; i < size; i++) {
    restart:
    {
      size_t idx = rand_r(seed) % upper;
      for (size_t j = 0; j < i; j++) {
        if (indices[j] == idx) {
          goto restart;
        }
      }
      indices[i] = idx;
    }
  }

  return indices;
}

//Run the array benchmark using the provided test array, its size, size of the multi word and the
//number of threads to be used.
void benchmark_array(const long *testData, int k, size_t arraySize, const int numThreads) {
  assert(testData != NULL);

  atomic_uintptr_t *localData = malloc(arraySize * sizeof(atomic_uintptr_t));
  for (size_t i = 0; i < arraySize; i++) {
    localData[i] = testData[i];
  }

  size_t casCounters[numThreads];
  size_t successCounters[numThreads];
  size_t numFreedObjects[numThreads];
  size_t numHelpedMcas[numThreads];
  size_t numSuccessfulCas[numThreads];
  size_t numFailedCas[numThreads];
  double averageFail[numThreads];

#pragma omp parallel default(shared) proc_bind(close)
  {
    int me = omp_get_thread_num();

#ifdef MEMORY_EPOCH
    //Initialize epochs
#pragma omp single
    {
      NUM_THREADS = omp_get_num_threads();
      OTHER_EPOCHS = malloc(NUM_THREADS * sizeof *OTHER_EPOCHS);
    }
    OTHER_EPOCHS[me] = &EPOCH;
    THREAD_ID = me;
#endif
#ifdef MEMORY_RC
    NUM_THREADS = (int16_t) numThreads;
#endif

    unsigned int seed = time(NULL);
    size_t counter = 0;
    size_t successful = 0;
    double accum = 0;
#pragma omp barrier

    while (accum < ONE_SECOND) {
      size_t *indices = array_rand_unique_naive(k, arraySize, &seed);

      struct timespec start;
      clock_gettime(CLOCK_MONOTONIC, &start);

      MCASDescriptor *desc = new_mcas(k, true);

      for (size_t i = 0; i < desc->size; i++) {
        size_t idx = indices[i];
        desc->words[i].address = &localData[idx];
        desc->words[i].old = read_mcas(&localData[idx]);
        desc->words[i].new = me;
      }
      if (mcas_apply(desc)) {
        //TODO Uncomment
        //ASSERT_EQUAL(desc->status, SUCCESSFUL)
        successful += 1;
      } else {
        //ASSERT_EQUAL(desc->status, FAILED)
      }
      struct timespec end;
      clock_gettime(CLOCK_MONOTONIC, &end);
      accum += TIME_DIFF_MS(start, end);

      free(indices);
      counter += 1;
    }
    casCounters[me] = counter;
    successCounters[me] = successful;
#ifdef MEMORY_RC
#pragma omp barrier
#pragma omp single
    {
      cleanup(localData, arraySize);
    }
    averageFail[me] = (double) CAS_TOTAL_RETRIES / (CAS_FAILED_RETRIES == 0 ? 1. : (double) CAS_FAILED_RETRIES);
    assert(CAS_FAILED_RETRIES != 0 || CAS_TOTAL_RETRIES == 0);
#else
    averageFail[me] = 0;
#endif
#ifdef MEMORY_EPOCH
#pragma omp barrier
    cleanup();
#pragma omp single
    {
      free(OTHER_EPOCHS);
    }
#endif
    numFreedObjects[me] = MCAS_FREED_OBJECTS;
    numHelpedMcas[me] = MCAS_HELPED;
    numSuccessfulCas[me] = MCAS_SUCCESSFUL_CAS;
    numFailedCas[me] = MCAS_FAILED_CAS;
  }

  free(localData);

  size_t total = 0;
  size_t totalSuccessful = 0;
  size_t totalFreedObjects = 0;
  size_t totalHelpedMcas = 0;
  size_t totalSuccessfulCas = 0;
  size_t totalFailedCas = 0;
  double averageRetryCount = 0.;
  for (size_t i = 0; i < numThreads; i++) {
    double ratio = (double) successCounters[i] / (double) casCounters[i];
    printf("Thread %zu executed %zu MCAS (%.4f %% successful)\n", i, casCounters[i], ratio * 100);

    total += casCounters[i];
    totalSuccessful += successCounters[i];
    totalFreedObjects += numFreedObjects[i];
    totalHelpedMcas += numHelpedMcas[i];
    totalSuccessfulCas += numSuccessfulCas[i];
    totalFailedCas += numFailedCas[i];
    averageRetryCount += averageFail[i];
  }
  averageRetryCount /= numThreads;

#if defined(MEMORY_RC) || defined(MEMORY_EPOCH)
  if (total != totalFreedObjects) {
    printf("Memory leaked (Executed %zu, freed %zu)\n", total, totalFreedObjects);
    abort();
  }
#endif

  printf("Executed %zu (%.4f %% successful) MCAS-operations within 1 second\n",
         total,
         (double) totalSuccessful * 100.0 / (double) total);
  printf("Freed %zu MCAS-Descriptors\n", totalFreedObjects);
  printf("Helped %zu MCAS-Descriptors (Ratio: %f)\n", totalHelpedMcas, (double) totalHelpedMcas / (double) total);
  printf("Successful/Failed CAS: %zu/%zu (Total: %zu, Ratio: %f)\n",
         totalSuccessfulCas,
         totalFailedCas,
         totalSuccessfulCas + totalFailedCas,
         (double) (totalSuccessfulCas + totalFailedCas) / (double) total);

  //Write results to file
  char buffer[4096];
  sprintf(buffer, "array%i_%zu", k, arraySize);
  FILE *f = NULL;
  if (access(buffer, F_OK) == 0) {
    //File already exists, append results
    f = fopen(buffer, "aw");
  } else {
    f = fopen(buffer, "w");
    //Add header
    fprintf(f, "commit,total_mcas,total_successful,num_threads,helped_mcas,total_cas,total_successful,average_retry\n");
  }

  fprintf(f,
          "%s,%zu,%zu,%i,%zu,%zu,%zu,%f\n",
          g_GIT_SHA1,
          total,
          totalSuccessful,
          numThreads,
          totalHelpedMcas,
          totalSuccessfulCas + totalFailedCas,
          totalSuccessfulCas,
          averageRetryCount);
  fclose(f);
}
#pragma clang diagnostic pop