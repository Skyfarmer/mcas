//
// Created by Simon Himmelbauer on 18.09.21.
//

#ifndef MCAS_SRC_BENCHMARK_H_
#define MCAS_SRC_BENCHMARK_H_

#include <stddef.h>

void benchmark_array(const long *testData, int k, size_t arraySize, int numThreads);

#endif //MCAS_SRC_BENCHMARK_H_
